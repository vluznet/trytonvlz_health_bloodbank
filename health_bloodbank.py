# -*- coding: utf-8 -*-

##############################################################################
#
#    GNU Health: The Free Health and Hospital Information System
#    Copyright (C) 2008-2017 Luis Falcon <lfalcon@gnusolidario.org>
#    Copyright (C) 2011-2017 GNU Solidario <health@gnusolidario.org>
#    Copyright (C) 2015 Cédric Krier
#    Copyright (C) 2014-2015 Chris Zimmerman <siv@riseup.net>
#
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from dateutil.relativedelta import relativedelta
from datetime import datetime, timedelta, date
from urllib.parse import urlencode
from urllib.parse import urlunparse
from collections import OrderedDict
from io import BytesIO

try:
    from PIL import Image
except ImportError:
    Image = None
from sql import Literal, Join

from trytond.model import ModelView, ModelSQL, fields, ValueMixin

__all__ = ['BloodBank']

class BloodBank(ModelSQL, ModelView):
    'Blood bank for tryton Health'
    __name__ = 'gnuhealth.bloodbank'

    rec_name = fields.Char('Codigo inicial', required=True)

    accepted_secuence = fields.Char('Secuencia sanguinea', readonly=True)
    patient = fields.Many2One('gnuhealth.patient', 'Paciente', required=True)
    blood_group = fields.Selection([
        (None, ''),
        ('a+', 'A+'),
        ('b+', 'B+'),
        ('ab+', 'AB+'),
        ('o+', 'O+'),
        ('a-', 'A-'),
        ('b-', 'B-'),
        ('ab-', 'AB-'),
        ('o-', 'O-'),
    ], 'Grupo Sanguineo', sort=False)

    skin_color = fields.Selection([
        (None, ''),
        ('mestizo', 'Mestizo'),
        ('blanco', 'Blanco'),
        ('negro', 'Negro'),
        ('asiatico', 'Asiatico'),
    ], 'Color de piel', sort=False)

    cardiac = fields.Char('Cardio pulmonar')
    hto = fields.Char('HTO')
    abdomen = fields.Char('Abdomen')
    comments = fields.Text('Comentarios')
    comments1 = fields.Text('Comentarios')
    others = fields.Text('Otros')
    state = fields.Selection((
        (None, ''),
        ('free', 'Free'),
        ('confirmed', 'Confirmed'),
        ('occupied', 'Occupied'),
        ('na', 'Not available'),
    ), 'Estado', readonly=True, sort=False)
    reg_date = fields.DateTime('Fecha de registro')

    date = fields.DateTime('Fecha')
    temp = fields.Char('Temp')
    arterial_pressure = fields.Char('Presion arterial')
    weight = fields.Char('Peso')
    cardiac_frec = fields.Char('Frecuencia Cardiaca')


    hb = fields.Char('HB')
    plaquet = fields.Char('Plaqueta')
    white_pellets = fields.Char('Globulos blancos')
    quest1 = fields.Boolean('Ha donado sangre alguna vez?')
    quest2 = fields.Boolean('Presento algun tipo de problema durante la ultima donacion de sangre?')
    quest3 = fields.Boolean('Ha sido transfundido en los ultimos 12 meses?')
    quest4 = fields.Boolean('Ha tomado algun medicamento en los ultimos 3 dias?')
    quest5 = fields.Boolean('Ha comido en las ultimas 3 horas?')
    quest6 = fields.Boolean('Ha usado drogas por via intravenosa o por otra via alguna vez?')
    quest7 = fields.Boolean('Ha tenido relaciones sexuales de riesgos con parejas iguales o direntes en los ultimos 6 meses?')
    quest8 = fields.Boolean('Ha salido del pais en los ultimos 6 meses?')

    quest9 = fields.Boolean('Sufre de dolor de pecho frecuente?')
    quest10 = fields.Boolean('Ha tenido o tiene problemas del corazon?')
    quest11 = fields.Boolean('Sufre de hipertension?')
    quest12 = fields.Boolean('Ha tenido hepatitis, alguna vez se ha puesto icterico, o ha estado en contacto con personas con hepatitis?')
